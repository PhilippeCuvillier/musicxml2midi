# musicxml2midi

Provides a script that converts MusicXML to MIDI, with an option to convert only some staves of the first MusicXML part.

Run `musicxml2midi.py` with `--help` argument to see usage.
