#!/usr/bin/env python3

from argparse import ArgumentParser
from music21 import converter, midi

def convert(input, output, only_tracks = None):
    print("Parsing input file…")
    musicxml = converter.parse(input)
    print("Converting input into MIDI…")
    midifile = midi.translate.streamToMidiFile(musicxml)

    # Erase tracks
    if only_tracks is not None and only_tracks > 0:
        N = only_tracks
        del midifile.tracks[1:N]
        del midifile.tracks[N+1:]

    print("Writing output file…")
    write(midifile, output)
    print("Done!")

def write(midifile, filename):
    midifile.open(filename, 'wb')
    midifile.write()
    midifile.close()

def main():
    """
    Run conversion from command line.
    """
    parser = ArgumentParser(description='Convert a MusicXML file to a MIDI file\n')
    parser.add_argument('-i', '--input', help='Input MusicXML file', required=True)
    parser.add_argument('-o', '--output', help='Output MIDI file', required=True)
    parser.add_argument('-t', '--only_tracks', help='Filter only one track, e.g., 1 for piano right hand, 2 for piano left hand (default: all tracks)', type=int, required=False)
    args = parser.parse_args()
    convert(args.input, args.output, args.only_tracks)

def test():
    input = "/Users/philippecuvillier/Documents/dev/musicxml2midi/example/058.xml"
    output = "/Users/philippecuvillier/Documents/dev/musicxml2midi/example/058.mid"
    N = None
    convert(input, output, N)

if __name__ == "__main__":
    main()
    # test()
